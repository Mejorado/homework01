
package exampartial3;

import java.util.Random;
import java.util.Scanner;


public class ExamPartial3 {

    
   public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        Random r = new Random();
        System.out.println("Ingrese el tamaño de la matriz(nxn)");
        int size = s.nextInt();
        int[][] mArray = new int[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                mArray[i][j] = (int) (r.nextFloat() * 90 + 10);
            }
            System.out.println("");
        }
        Boolean runAgain = true;
        while (runAgain) {
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    System.out.print(mArray[i][j] + " ");
                }
                System.out.println("");
            }
            System.out.println("Que desea hacer? \n 1.- Imprimir columna \n 2.- Imprimir fila \n 3.- Imprimir espacio especifico (coordenadas) \n 4.- Diagonal principal \n 5.- Diagonal Inversa  ");
            int choose = s.nextInt();
            switch (choose) {
                case (1): {
                    System.out.println("Que columna desea imprimir? (0 - " + (size - 1) + ")");
                    int column = s.nextInt();
                    for (int i = 0; i < size; i++) {
                        for (int j = 0; j < size; j++) {
                            if (j == column) {
                                System.out.print(mArray[i][j] + " ");
                            }
                        }
                        System.out.println("");
                    }
                    break;
                }
                case (2): {
                    System.out.println("Que fila desea imprimir? (0 - " + (size - 1) + ")");
                    int row = s.nextInt();
                    for (int i = 0; i < size; i++) {
                        for (int j = 0; j < size; j++) {
                            if (i == row) {
                                System.out.print(mArray[i][j] + " ");
                            }
                        }
                        System.out.println("");
                    }
                    break;
                }
                case (3): {
                    System.out.println("Ingrese las coordenadas (0 - " + (size - 1) + ")");
                    int row = s.nextInt();
                    int column = s.nextInt();
                    System.out.println("El numero es: " + mArray[row][column]);
                    break;
                }
                case (4): {
                    for (int i = 0; i < size; i++) {
                        for (int j = 0; j < size; j++) {
                            if (i == j) {
                                System.out.print(mArray[i][j] + " ");
                            }
                        }
                        System.out.println("");
                    }
                    break;
                }
                case (5): {
                    int columnPosition = size - 1;
                    for (int i = 0; i < size; i++) {
                        for (int j = 0; j < size; j++) {
                            if (j == columnPosition) {
                                System.out.print(mArray[i][j] + " ");
                                columnPosition--;
                            }
                        }
                        System.out.println("");
                    }
                    break;
                }
                case (6): {
                    
                    for (int i = 0; i < size; i++) {
                        for (int j = 0; j < size; j++) {
                            if (j != i) {
                                System.out.print(mArray[i][j] + " ");
                            }
                        }
                        System.out.println("");
                    }
                    break;
                }
                
            }
            System.out.println("Desea seleccionar otra opción? true/false");
            runAgain = s.nextBoolean();
        }
    }

}
